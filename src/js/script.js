let calendar = document.querySelector('.calendar');
let btn = document.querySelector('.btn-submit');

/* --------------creation calendar-------------- */
let currentDate = new Date();
let date = new Date();
let currentYear = date.getFullYear();
let monthArray = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
let weekDayArray = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'];

function createElem(tagName, className, text) {
  let element = document.createElement(tagName);
  element.classList.add(className);
  if (text) {
    element.textContent = text;
  }
  return element;
}
for (let i = 0; i < 12; i++) {
  date.setMonth(i);

  let calendarElem = createElem('div', 'calendar-item');
  let monthName = createElem('div', 'month-name', monthArray[i]);

  let week = createElem('div', 'week');
  for (let i = 0; i < 7; i++) {
    let weekDay = createElem('div', 'week--day', weekDayArray[i]);
    if (i > 4) {
      weekDay.classList.add('holiday');
    }
    week.appendChild(weekDay);
  }

  let monthDays = createElem('div', 'month-days');
  let lastDay = (new Date(currentYear, i + 1, 0)).getDate();
  let firstDay = (new Date(currentYear, date.getMonth(), 1).getDay());
  if (firstDay === 0) { firstDay = 7 };

  let flag = 0;
  let j = 1;
  for (let i = 1; i <= lastDay; i++) {
    
    let monthDaysDay = createElem('div', 'month-days--day', j)
    let emptyDay = createElem('div', 'month-days--day', 0)
    if (j === currentDate.getDate() && date.getMonth() === currentDate.getMonth()) {
      monthDaysDay.classList.add('today');
    }

    if (i === firstDay || flag === 1) {
      monthDays.appendChild(monthDaysDay);
      flag = 1;
      j++;
    } else if (i != firstDay && flag === 0) {
      monthDays.appendChild(emptyDay);
      lastDay++;
    }

  }
  calendarElem.appendChild(monthName);
  calendarElem.appendChild(week);
  calendarElem.appendChild(monthDays);
  calendar.appendChild(calendarElem);
}
/* --------------creation calendar-------------- */


/* ----------------------choosing date---------------------- */
let result = {};
let isChoosenDay = 0;
let dayMonth = document.querySelectorAll('.month-days--day');
dayMonth.forEach(function (item) {
  item.addEventListener('click', () => {
    if (item.classList.contains('choosen')) {
      item.classList.remove('choosen');
      isChoosenDay = document.querySelectorAll('.choosen');
      if (isChoosenDay.length === 0) { btn.classList.add('disabled') }
    } else if (!item.classList.contains('choosen')) {
      item.classList.add('choosen');
      isChoosenDay = document.querySelectorAll('.choosen');
      if (phoneFlag > 0 && isChoosenDay.length > 0) {
        if (btn.classList.contains('disabled')) {
          btn.classList.remove('disabled')

          btn.addEventListener('click', () => {
            let items = document.querySelectorAll('.calendar-item');
            items.forEach((item) => {
              let monthName = item.querySelector('.month-name');
              let choosenDays = item.querySelectorAll('.choosen');
              let dayArray = [];
              choosenDays.forEach((day) => {
                dayArray.push(day.textContent);
                result[monthName.textContent] = dayArray;
                
              })
              
            })
            result['Phone number'] = phone.value;
            alert('Выбранные даты забронированы (подробности в консоли)');
            console.log(result);
          })

        }
      }
    }
  });
})
/* ----------------------choosing date---------------------- */
let phoneFlag = 0;



